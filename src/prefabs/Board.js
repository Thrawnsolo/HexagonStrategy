import Phaser from 'phaser';

const Board = function (state, grid) {
    Phaser.Group.call(this, state.game);
    const { GAME_CONSTANTS } = state;
    let x;
    let y;
    this.state = state;
    this.grid = grid;
    this.rows = grid.length;
    this.cols = grid[0].length;
    this.terrains = [
        { asset: 'grass2' },
        { asset: 'water', blocked: true },
        { asset: 'rocks' },
        { asset: 'grassTree' },
        { asset: 'grassTrees' }
    ];
    for (let row = 0; row < this.rows; row++) {
        for (let col = 0; col < this.cols; col++) {
            if (row % 2 === 0) {
                x = GAME_CONSTANTS.MARGIN_X + (col * GAME_CONSTANTS.TILE_W);
            } else {
                x = GAME_CONSTANTS.MARGIN_X + (col * GAME_CONSTANTS.TILE_W) + (GAME_CONSTANTS.TILE_W / 2);
            }
            y = GAME_CONSTANTS.MARGIN_Y + (row * (GAME_CONSTANTS.TILE_H * (3 / 4)));
            const tile = new Phaser.Sprite(this.game, x, y, this.terrains[grid[row][col]].asset);
            tile.row = row;
            tile.col = col;
            tile.terrainAsset = this.terrains[grid[row][col]].asset;
            tile.blocked = this.terrains[grid[row][col]].blocked;
            tile.inputEnabled = true;
            tile.input.pixelPerfectClick = true;
            /* tile.events.onInputDown.add(() => {
                const adjacentTiles = board.getAdjacent(tile, true);
                adjacentTiles.forEach((t) => {
                    const adjacentTile = t;
                    adjacentTile.alpha = 0.3;
                });
            }); */
            this.add(tile);
        }
    }
};

Board.prototype = Object.create(Phaser.Group.prototype);
Board.prototype.constructor = Board;

Board.prototype.getFromRowCol = function (row, col) {
    let foundTile;
    this.forEach((tileElement) => {
        if (tileElement.row === row && tileElement.col === col) {
            foundTile = tileElement;
        }
    });
    return foundTile;
};

Board.prototype.getAdjacent = function (selectedTile, rejectBlocked) {
    const adjacentTiles = [];
    const { row, col } = selectedTile;
    let adjacentTile;
    // Relative positions depend on wether the row is odd or even
    const relativePositions = (row % 2 === 0) ?
        [
            { r: -1, c: 0 },
            { r: -1, c: -1 },
            { r: 0, c: -1 },
            { r: 0, c: 1 },
            { r: 1, c: 0 },
            { r: 1, c: -1 }
        ] : [
            { r: -1, c: 0 },
            { r: -1, c: 1 },
            { r: 0, c: -1 },
            { r: 0, c: 1 },
            { r: 1, c: 0 },
            { r: 1, c: 1 }
        ];

    relativePositions.forEach((position) => {
        if ((row + position.r >= 0) && (row + position.r < this.rows) &&
            (col + position.c >= 0) && (col + position.c < this.cols)) {
            adjacentTile = this.getFromRowCol(row + position.r, col + position.c);
            if (!rejectBlocked || !adjacentTile.blocked) {
                adjacentTiles.push(adjacentTile);
            }
        }
    });

    return adjacentTiles;
};

Board.prototype.getXYFromRowCol = function (row, col) {
    const pos = {};
    const { GAME_CONSTANTS } = this.state;
    if (row % 2 === 0) {
        pos.x =
            GAME_CONSTANTS.MARGIN_X + (col * GAME_CONSTANTS.TILE_W) + (GAME_CONSTANTS.TILE_W / 2);
    } else {
        pos.x = GAME_CONSTANTS.MARGIN_X + (col * GAME_CONSTANTS.TILE_W) + (GAME_CONSTANTS.TILE_W);
    }
    pos.y =
        GAME_CONSTANTS.MARGIN_Y + (row * (GAME_CONSTANTS.TILE_H * (3 / 4))) + (GAME_CONSTANTS.TILE_H / 2);
    return pos;
};

export default Board;
