import Phaser from 'phaser';

const Unit = function (state, data) {
    const position = state.board.getXYFromRowCol(data.row, data.col);
    Phaser.Sprite.call(this, state.game, position.x, position.y, data.asset);
    this.anchor.setTo(0.5);
    this.game = state.game;
    this.state = state;
    this.board = state.board;
    this.row = data.row;
    this.col = data.col;
    this.data = data;
};

Unit.prototype = Object.create(Phaser.Sprite.prototype);
Unit.prototype.constructor = Unit;

Unit.prototype.showMovementOptions = function () {
    this.state.clearSelection();
    if (this.state.uiBlocked) {
        return null;
    }
    const currTile = this.board.getFromRowCol(this.row, this.col);
    const adjacentCells = this.board.getAdjacent(currTile, true);
    adjacentCells.forEach((tile) => {
        const tileElement = tile;
        tileElement.alpha = 0.7;
        tile.events.onInputDown.add(this.moveUnit, this);
    });
    return true;
};

Unit.prototype.moveUnit = function (tile) {
    this.state.clearSelection();
    this.state.uiBlocked = true;

    const pos = this.board.getXYFromRowCol(tile.row, tile.col);
    const unitMovement = this.game.add.tween(this);
    unitMovement.to(pos, 200);
    unitMovement.onComplete.add(() => {
        this.state.uiBlocked = false;
        this.row = tile.row;
        this.col = tile.col;
        // Check for battles
        this.checkBattle();
        this.state.checkGameEnd();
        // Prepare next unit
        this.state.prepareNextUnit();
    });
    unitMovement.start();
};

Unit.prototype.attack = function (attackedUnit) {
    const attacker = this;
    const attacked = attackedUnit;
    const damagedAttacked = Math.max(
        0,
        (attacker.data.attack * Math.random()) - (attacked.data.defense * Math.random())
    );
    const damagedAttacker = Math.max(
        0,
        (attacked.data.attack * Math.random()) - (attacker.data.defense * Math.random())
    );
    attacked.data.health -= damagedAttacked;
    attacker.data.health -= damagedAttacker;
    if (attacked.data.health <= 0) {
        attacked.kill();
    }
    if (attacker.data.health <= 0) {
        attacker.kill();
    }
};

Unit.prototype.checkBattle = function () {
    const rivalUnits = this.isPlayer ? this.state.enemyUnits : this.state.playerUnits;
    let fightUnit;
    rivalUnits.forEachAlive((unit) => {
        if (this.row === unit.row && this.col === unit.col) {
            fightUnit = unit;
        }
    });

    if (typeof fightUnit !== 'undefined') {
        while (this.data.health >= 0 && fightUnit.data.health >= 0) {
            this.attack(fightUnit);
        }
    }
};

Unit.prototype.playTurn = function () {
    if (this.isPlayer) {
        this.showMovementOptions();
    } else {
        this.aiEnemyMovement();
    }
};

Unit.prototype.aiEnemyMovement = function () {
    // clear previous selection
    this.state.clearSelection();
    // get current tile and adjacent tiles
    const currentTile = this.board.getFromRowCol(this.row, this.col);
    const adjacentCells = this.board.getAdjacent(currentTile, true);
    let targetTile;
    adjacentCells.forEach((tile) => {
        this.state.playerUnits.forEachAlive((playerUnit)=> {
            if (playerUnit.col === tile.col && playerUnit.row === tile.row) {
                targetTile = tile;
            }
        });
    });
    if (typeof targetTile === 'undefined') {
        const randomIndex = Math.floor(Math.random() * adjacentCells.length);
        targetTile = adjacentCells[randomIndex];
    }
    this.moveUnit(targetTile);
};

export default Unit;

