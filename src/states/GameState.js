import Phaser from 'phaser';
import Unit from '../prefabs/Unit';
import Board from '../prefabs/Board';
import enemyUnitsData from '../../assets/data/enemyUnits.json';
import mapData from '../../assets/data/map.json';
import playerUnitsData from '../../assets/data/playerUnits.json';

const gameData = {
    enemy: enemyUnitsData,
    map: mapData,
    player: playerUnitsData
};

export default () => {
    const gameState = new Phaser.State();
    gameState.init = function () {
        this.GAME_CONSTANTS = {
            TILE_W: 56,
            TILE_H: 64,
            MARGIN_X: 30,
            MARGIN_Y: 5
        };
    };

    gameState.create = function () {
        this.map = mapData;
        this.board = new Board(this, this.map.grid);
        this.places = this.add.group();
        this.playerUnits = this.add.group();
        this.enemyUnits = this.add.group();
        this.initUnits();
        this.initPlaces();
        // run turn
        this.newTurn();
    };

    gameState.initUnits = function () {
        gameData.player.forEach((unitData) => {
            const playerUnit = new Unit(this, unitData);
            playerUnit.isPlayer = true;
            this.playerUnits.add(playerUnit);
        });
        gameData.enemy.forEach((unitData) => {
            this.enemyUnits.add(new Unit(this, unitData));
        });
    };

    gameState.clearSelection = function () {
        this.board.setAll('alpha', 1);
        this.board.forEach((tile) => {
            tile.events.onInputDown.removeAll();
        });
    };

    gameState.newTurn = function () {
        this.allUnits = [];
        this.playerUnits.forEachAlive(unit => this.allUnits.push(unit));
        this.enemyUnits.forEachAlive(unit => this.allUnits.push(unit));
        // randomize the array
        this.shuffle(this.allUnits);
        this.currentUnitIndex = 0;
        this.prepareNextUnit();
    };

    gameState.shuffle = function (arrayElement) {
        let counter = arrayElement.length;
        const array = arrayElement;
        let temp;
        let index;
        while (counter > 0) {
            index = Math.floor(Math.random() * counter);
            counter--;
            temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }
        return array;
    };

    gameState.prepareNextUnit = function () {
        if (this.currentUnitIndex < this.allUnits.length) {
            const unit = this.allUnits[this.currentUnitIndex];
            this.currentUnitIndex++;
            if (unit.alive) {
                unit.playTurn();
            } else {
                this.prepareNextUnit();
            }
        } else {
            this.newTurn();
        }
    };

    gameState.initPlaces = function () {
        const position = this.board.getXYFromRowCol(
            this.map.playerBase.row,
            this.map.playerBase.col
        );
        this.playerBase = new Phaser.Sprite(
            this.game,
            position.x,
            position.y,
            this.map.playerBase.asset
        );
        this.playerBase.anchor.setTo(0.5);
        this.playerBase.row = this.map.playerBase.row;
        this.playerBase.col = this.map.playerBase.col;
        this.places.add(this.playerBase);

        const enemyPosition = this.board.getXYFromRowCol(
            this.map.enemyBase.row,
            this.map.enemyBase.col
        );
        this.enemyBase = new Phaser.Sprite(
            this.game,
            enemyPosition.x,
            enemyPosition.y,
            this.map.enemyBase.asset
        );
        this.enemyBase.anchor.setTo(0.5);
        this.enemyBase.row = this.map.enemyBase.row;
        this.enemyBase.col = this.map.enemyBase.col;
        this.places.add(this.enemyBase);
    };

    gameState.checkGameEnd = function () {
        const unit = this.allUnits[this.currentUnitIndex - 1];
        if (unit.isPlayer) {
            if (unit.row === this.enemyBase.row && unit.col === this.enemyBase.col) {
                console.log('You won');
            }
        } else if (unit.row === this.playerBase.row && unit.col === this.playerBase.col) {
            console.log('You lost');
        }
    };

    return gameState;
};
