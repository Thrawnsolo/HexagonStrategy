import Phaser from 'phaser';

import black from '../../assets/images/black.png';
import darkTemple from '../../assets/images/dark-temple.png';
import fire from '../../assets/images/fire.png';
import grass from '../../assets/images/grass.png';
import grass2 from '../../assets/images/grass2.png';
import grassTree from '../../assets/images/grass3.png';
import grassTrees from '../../assets/images/grass4.png';
import house from '../../assets/images/house-1.png';
import sacredWarrior from '../../assets/images/leopard-warrior.png';
import ogre from '../../assets/images/ogre.png';
import orc from '../../assets/images/orc.png';
import rocks from '../../assets/images/rocks.png';
import warrior from '../../assets/images/warrior.png';
import water from '../../assets/images/water.png';
import wolf from '../../assets/images/wolf.png';


const imagesAssets = [
    black,
    darkTemple,
    fire,
    grass,
    grass2,
    grassTree,
    grassTrees,
    house,
    sacredWarrior,
    ogre,
    orc,
    rocks,
    warrior,
    water,
    wolf
];
const imagesNames = ['black', 'darkTemple', 'fire', 'grass', 'grass2', 'grassTree', 'grassTrees', 'house',
    'sacredWarrior', 'ogre', 'orc', 'rocks', 'warrior', 'water', 'wolf'];

export default () => {
    const obj = new Phaser.State();
    obj.preload = function () {
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
        this.preloadBar.anchor.setTo(0.5);
        this.preloadBar.scale.setTo(100, 1);
        this.load.setPreloadSprite(this.preloadBar);

        this.load.images(imagesNames, imagesAssets);
    };
    obj.create = function () {
        this.state.start('Game');
    };
    return obj;
};
