import Phaser from 'phaser';
import bar from '../../assets/images/preloader-bar.png';

export default () => {
    const bootState = new Phaser.State();
    bootState.init = function () {
        this.game.stage.backgroundColor = '#fff';
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
    };
    bootState.preload = function () {
        this.load.image('bar', bar);
    };
    bootState.create = function () {
        this.state.start('Preload');
    };
    return bootState;
};
